package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Curso implements Comparable<Curso>
{
    /* Atributos **************************************************************/

    private int id;
    private String titulo;
    private double horas;
    
    /* Constructores **********************************************************/

    public Curso() {
        id=0;
        titulo="";
        horas=0.0;
    }

    /* Métodos getters & setters **********************************************/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

    /* Métodos ****************************************************************/

    public boolean existeCurso(ConexionBD bd) throws Exception {
        try {
            String sql="SELECT count(*) FROM Cursos WHERE "+
                        "id="+id;
            ResultSet rs=bd.getSt().executeQuery(sql);
            rs.next();
            int n=rs.getInt(1);
            if (n>0) return true;
        } catch (SQLException e) {
            throw new Exception("Error existeCurso()!!",e);
        }
        return false;
    }
    
    public void altaCurso(ConexionBD bd) throws Exception {
        if (existeCurso(bd)) throw new Exception("El curso ya existe!!");
        try {
            String sql="INSERT INTO Cursos VALUES ("+
                        id+",'"+
                        titulo+"',"+
                        horas+")";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error altaCurso()!!",e);
        }
    }

    public void bajaCurso(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) throw new Exception("El curso no existe!!");
        try {
            String sql="DELETE FROM Cursos WHERE "+
                        "id="+id;
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error bajaCurso()!!",e);
        }
    }

    public static void listadoCursos(ConexionBD bd, List<Curso> t) throws Exception {
        try {
            String sql="SELECT * FROM Cursos";
            ResultSet rs=bd.getSt().executeQuery(sql);
            Curso c;
            while (rs.next()) {
                c=new Curso();
                c.setId(rs.getInt("id"));
                c.setTitulo(rs.getString("titulo"));
                c.setHoras(rs.getDouble("horas"));
                t.add(c);
            }
        } catch (SQLException e) {
            throw new Exception("Error listadoCursos()!!",e);
        }
    }
    
    public static ResultSet listadoCursosGUI(ConexionBD bd) throws SQLException{
        ResultSet rs = null;
        String sql="SELECT * FROM CURSOS";
        
        rs = bd.getSt().executeQuery(sql);
        return rs;
        
    }
    
    @Override
    public int compareTo(Curso o) {
        if (this.getId()==o.getId())
            return 0;
        else if (this.getId()>o.getId())
            return 1;
        else return -1;
    }

}
